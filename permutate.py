class permutate_t:
    def __init__(self, list_of_list_of_objects):
        self.database = list_of_list_of_objects
        self.count = len(self.database)
        self.curr_counts = None
        self.max_counts = [0]*self.count

        for index in range(self.count):
            self.max_counts[index] = len(self.database[index])

    def get_next_permutation(self):
        if self.curr_counts==None:
            self.curr_counts = [0]*self.count
        else:
            ret = self._increment_index(0)
            if ret==False:
                # We have exhausted every possible permutation
                return None
        return self._get_permutation_for_given_curr_counts()


    def _increment_index(self, index):
        if index==self.count:
            return False
        self.curr_counts[index] += 1
        if self.curr_counts[index]==self.max_counts[index]:
            self.curr_counts[index] = 0
            return self._increment_index(index+1)
        else:
            return True

    def get_nth_permutation(self, n):
        # TODO
        pass


    def _get_permutation_for_given_curr_counts(self):
        l = [None]*self.count
        for index in range(self.count):
            l_index = self.curr_counts[index]
            l[index] = self.database[index][l_index]
        return l



if __name__=="__main__":
    list_of_list = [
            ["D", "G"],
            ["R", "G"],
            ["S", "Y"],
            ["G"],
            ["G"],
            ["G", "S"],
            ["H", "L", "F", "Y"],
            ["I", "L", "M", "F"],
            ["D", "C", "G", "F", "Y", "V"],
            ["R", "G", "W"],
            ["D", "C", "G", "Y"],
            ["D", "F", "Y", "V"],
            ["D", "F", "Y", "V"],
            ["N", "I", "F", "Y"],
            ["A", "D", "F", "S", "Y", "V"],
            ["F", "S", "Y"],
            ["F", "S", "Y"],
            ]
    permutate = permutate_t(list_of_list)
    while True:
        x = permutate.get_next_permutation()
        if x==None:
            break
        print("".join(x))

    print("Finished")
